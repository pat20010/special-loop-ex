function draw1 (n) {
  console.log('ข้อ 1')
  validateInput(n)
  let result = ''
  for (let index = 0; index < n; index++) {
    result += '*'
  }
  console.log(result)
}

function draw2 (n) {
  console.log('ข้อ 2')
  validateInput(n)
  let result = ''
  let count = n
  let valueN = n
  while (valueN > 0 && count > 0) {
    result += '*'
    valueN--
    if (valueN === 0) {
      console.log(result)
      result = ''
      count--
      if (count > 0) {
        valueN = n
      }
    }
  }
}

function draw3 (n) {
  console.log('ข้อ 3')
  validateInput(n)
  let result = ''
  for (let index = 0; index < n; index++) {
    let number = index + 1
    result += number
    if (number === n) {
      for (let indexPrint = 0; indexPrint < n; indexPrint++) {
        console.log(result)
      }
    }
  }
}

function draw4 (n) {
  console.log('ข้อ 4')
  validateInput(n)
  for (let index = 0; index < n; index++) {
    let result = ''
    let number = index + 1
    result += number
    for (let indexPrint = 1; indexPrint < n; indexPrint++) {
      result += number
      if (indexPrint + 1 === n) {
        console.log(result)
      }
    }
  }
}

function draw5 (n) {
  console.log('ข้อ 5')
  validateInput(n)
  for (let index = n; index > 0; index--) {
    let result = ''
    let number = index
    result += number
    for (let indexPrint = 1; indexPrint < n; indexPrint++) {
      result += number
      if ((indexPrint + 1) === n) {
        console.log(result)
      }
    }
  }
}

function draw6 (n) {
  console.log('ข้อ 6')
  validateInput(n)
  let number = 0
  for (let index = 0; index < n; index++) {
    let result = ''
    for (let indexPrint = 0; indexPrint < n; indexPrint++) {
      number = number + 1
      result += number
      if ((indexPrint + 1) === n) {
        console.log(result)
      }
    }
  }
}

function draw7 (n) {
  console.log('ข้อ 7')
  validateInput(n)
  let number = (n * n)
  for (let index = 0; index < n; index++) {
    let result = ''
    for (let indexPrint = 0; indexPrint < n; indexPrint++) {
      result += number
      number = number - 1
      if ((indexPrint + 1) === n) {
        console.log(result)
      }
    }
  }
}

function draw8 (n) {
  console.log('ข้อ 8')
  validateInput(n)
  let result = ''
  let sum = 0
  for (let index = 0; index < n; index++) {
    result = sum
    console.log(result)
    sum += 2
  }
}

function draw9 (n) {
  console.log('ข้อ 9')
  validateInput(n)
  let result = ''
  let sum = 0
  for (let index = 0; index < n; index++) {
    result = sum += 2
    console.log(result)
  }
}

function draw10 (n) {
  console.log('ข้อ 10')
  validateInput(n)
  for (let index = 0; index < n; index++) {
    let result = ''
    let sum = index + 1
    for (let indexPrint = 0; indexPrint < n; indexPrint++) {
      let plus = index + 1
      result += sum
      if (indexPrint + 1 === n) {
        console.log(result)
      }
      sum += plus
    }
  }
}

function draw11 (n) {
  console.log('ข้อ 11')
  validateInput(n)
  let result = ''
  let under = 1
  let count = 0
  while (count < n && under <= n) {
    if (count + 1 === under) {
      result += '_'
    } else {
      result += '*'
    }
    count++
    if (count === n) {
      console.log(result)
      result = ''
      count = 0
      under++
    }
  }
}

function draw12 (n) {
  console.log('ข้อ 12')
  validateInput(n)
  let result = ''
  let under = n
  let count = 0
  while (count < n && under > 0) {
    if (count + 1 === under) {
      result += '_'
    } else {
      result += '*'
    }
    count++
    if (count === n) {
      console.log(result)
      result = ''
      count = 0
      under--
    }
  }
}

function draw13 (n) {
  console.log('ข้อ 13')
  validateInput(n)
  let result = ''
  let under = 1
  let count = 0
  while (count < n && under <= n) {
    if (count + 1 > under) {
      result += '_'
    } else {
      result += '*'
    }
    count++
    if (count === n) {
      console.log(result)
      result = ''
      count = 0
      under++
    }
  }
}

function draw14 (n) {
  console.log('ข้อ 14')
  validateInput(n)
  let result = ''
  let under = n
  let count = 0
  while (count < n && under > 0) {
    if (count + 1 > under) {
      result += '_'
    } else {
      result += '*'
    }
    count++
    if (count === n) {
      console.log(result)
      result = ''
      count = 0
      under--
    }
  }
}

function draw15 (n) {
  console.log('ข้อ 15')
  validateInput(n)
  let result = ''
  let symbol1 = 0
  let symbol2 = n
  for (let index = 1; index < n * 2; index++) {
    for (let indexPrint = 0; indexPrint < n; indexPrint++) {
      if (symbol1 < n) {
        if (symbol1 >= indexPrint) {
          result += '*'
        } else {
          result += '-'
        }
      }
      if (symbol2 < n) {
        if (symbol2 <= indexPrint) {
          result += '-'
        } else {
          result += '*'
        }
      }
    }
    symbol1++
    if (symbol1 >= n) {
      symbol2--
    }
    console.log(result)
    result = ''
  }
}

function draw16 (n) {
  console.log('ข้อ 16')
  validateInput(n)
  let result = ''
  let symbol1 = 0
  let symbol2 = n
  for (let index = 1; index < n * 2; index++) {
    for (let indexPrint = 0; indexPrint < n; indexPrint++) {
      if (symbol1 < n) {
        if (symbol1 >= indexPrint) {
          result += symbol1 + 1
        } else {
          result += '-'
        }
      }
      if (symbol2 < n) {
        if (symbol2 <= indexPrint) {
          result += '-'
        } else {
          result += symbol2
        }
      }
    }
    symbol1++
    if (symbol1 >= n) {
      symbol2--
    }
    console.log(result)
    result = ''
  }
}

function draw17 (n) {
  console.log('ข้อ 17')
  validateInput(n)
  let result = ''
  let symbol1 = n - 1
  for (let index = 0; index < n; index++) {
    for (let indexPrint = 0; indexPrint < n; indexPrint++) {
      if (symbol1 > indexPrint) {
        result += '-'
      } else {
        result += '*'
      }
    }
    symbol1--
    console.log(result)
    result = ''
  }
}

function draw18 (n) {
  console.log('ข้อ 18')
  validateInput(n)
  let result = ''
  let symbol1 = 0
  for (let index = 0; index < n; index++) {
    for (let indexPrint = 0; indexPrint < n; indexPrint++) {
      if (symbol1 > indexPrint) {
        result += '-'
      } else {
        result += '*'
      }
    }
    symbol1++
    console.log(result)
    result = ''
  }
}

function draw19 (n) {
  console.log('ข้อ 19')
  validateInput(n)
  let result = ''
  let symbol1 = n - 1
  let symbol2 = 0
  for (let index = 1; index < n * 2; index++) {
    for (let indexPrint = 0; indexPrint < n; indexPrint++) {
      if (symbol1 >= 0) {
        if (symbol1 > indexPrint) {
          result += '-'
        } else {
          result += '*'
        }
      }

      if (symbol2 > 0) {
        if (symbol2 > indexPrint) {
          result += '-'
        } else {
          result += '*'
        }
      }
    }
    symbol1--
    if (symbol1 < 0) {
      symbol2++
    }
    console.log(result)
    result = ''
  }
}

function draw20 (n) {
  console.log('ข้อ 20')
  validateInput(n)
  let result = ''
  let symbol1 = n - 1
  let symbol2 = 0
  let count = 1
  for (let index = 1; index < n * 2; index++) {
    for (let indexPrint = 0; indexPrint < n; indexPrint++) {
      if (symbol1 >= 0) {
        if (symbol1 > indexPrint) {
          result += '-'
        } else {
          result += count
          count++
        }
      }

      if (symbol2 > 0) {
        if (symbol2 > indexPrint) {
          result += '-'
        } else {
          result += count
          count++
        }
      }
    }
    symbol1--
    if (symbol1 < 0) {
      symbol2++
    }
    console.log(result)
    result = ''
  }
}

function draw21 (n) {
  console.log('ข้อ 21')
  validateInput(n)
  let result = ''
  let left = n
  let right = n
  for (let index = 0; index < n; index++) {
    for (let indexPrint = 1; indexPrint < n * 2; indexPrint++) {
      if (left >= indexPrint && right <= indexPrint) {
        result += '*'
      } else {
        result += '-'
      }
    }
    left++
    right--
    console.log(result)
    result = ''
  }
}

function draw22 (n) {
  console.log('ข้อ 22')
  validateInput(n)
  let result = ''
  let left = 0
  let right = n * 2
  for (let index = 0; index < n; index++) {
    for (let indexPrint = 1; indexPrint < n * 2; indexPrint++) {
      if (left < indexPrint && right > indexPrint) {
        result += '*'
      } else {
        result += '-'
      }
    }
    left++
    right--
    console.log(result)
    result = ''
  }
}

function draw23 (n) {
  console.log('ข้อ 23')
  validateInput(n)
  let result = ''
  let left = n
  let right = n
  let count = 0
  for (let index = 1; index < n * 2; index++) {
    if (count < n) {
      for (let indexPrint = 1; indexPrint < n * 2; indexPrint++) {
        if (left >= indexPrint && right <= indexPrint) {
          result += '*'
        } else {
          result += '-'
        }
      }
      left++
      right--
      if (right === 0) {
        left = 1
        right = (n * 2) - 1
      }
    }
    if (count >= n) {
      for (let indexPrint = 1; indexPrint < n * 2; indexPrint++) {
        if (left < indexPrint && right > indexPrint) {
          result += '*'
        } else {
          result += '-'
        }
      }
      left++
      right--
    }
    count++
    console.log(result)
    result = ''
  }
}

function draw24 (n) {
  console.log('ข้อ 24')
  validateInput(n)
  let result = ''
  let left = n
  let right = n
  let round = 0
  let count = 1
  for (let index = 1; index < n * 2; index++) {
    if (round < n) {
      for (let indexPrint = 1; indexPrint < n * 2; indexPrint++) {
        if (left >= indexPrint && right <= indexPrint) {
          result += count
          count++
        } else {
          result += '-'
        }
      }
      left++
      right--
      if (right === 0) {
        left = 1
        right = (n * 2) - 1
      }
    }
    if (round >= n) {
      for (let indexPrint = 1; indexPrint < n * 2; indexPrint++) {
        if (left < indexPrint && right > indexPrint) {
          result += count
          count++
        } else {
          result += '-'
        }
      }
      left++
      right--
    }
    round++
    console.log(result)
    result = ''
  }
}

function validateInput (input) {
  if (typeof input !== 'number') {
    throw new Error('Invalid input type')
  }
}

module.exports = {
  draw1,
  draw2,
  draw3,
  draw4,
  draw5,
  draw6,
  draw7,
  draw8,
  draw9,
  draw10,
  draw11,
  draw12,
  draw13,
  draw14,
  draw15,
  draw16,
  draw17,
  draw18,
  draw19,
  draw20,
  draw21,
  draw22,
  draw23,
  draw24
}
